此流程即为模拟参与开源项目时候的代码提交流程

1. 首先fork一下仓库
2. git clone `仓库` 到本地
3. git remote add origin `你的仓库地址`
4. git remote add upstream `https://gitee.com/cybertheye/attackonarchitect.git`
5. 作业，分享的资料，push，到远程仓库，在Pull Requests 中 点击新建 Pull Request 
6. 同步其他人的提交
7. git fetch upstream
8. git rebase upstream/master （冲突合并 git rebase continue)
9. git push -u origin master 
